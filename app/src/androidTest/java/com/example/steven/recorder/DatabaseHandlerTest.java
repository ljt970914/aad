package com.example.steven.recorder;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@SmallTest

public class DatabaseHandlerTest {

    private DatabaseHandler dbtest1;

    @Before
    public void setBill() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dbtest1 = new DatabaseHandler(context);

    }

    @Test
    public void TestAddBill() {
        dbtest1.addBill(new Bill("+", "MILK", "1/1/2019", 4.0, "TestItem1"));
        dbtest1.addBill(new Bill("+", "Tea", "1/1/2019", 5.0, "TestItem2"));
        dbtest1.addBill(new Bill("-", "Coffee", "1/1/2019", 6.0, "TestItem3"));
    }
}
