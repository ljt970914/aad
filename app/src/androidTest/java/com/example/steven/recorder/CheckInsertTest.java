package com.example.steven.recorder;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@SmallTest

public class CheckInsertTest {
    private DatabaseHandler dbtest0;

    @Before
    public void setBill() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dbtest0 = new DatabaseHandler(context);
    }

    @Test
    public void TestInsert() {
        dbtest0.check(1);
        assertEquals(dbtest0.ResultOfName, "MILK");
    }
}