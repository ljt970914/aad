package com.example.steven.recorder;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@SmallTest

public class SumTest {
    private DatabaseHandler dbtest2;

    @Before
    public void setBill() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dbtest2 = new DatabaseHandler(context);
    }

    @Test
    public void sumTest() {
        dbtest2.sum(0.0);
        assertEquals(dbtest2.strIncome, "9.0");
        assertEquals(dbtest2.strExpen, "6.0");
        assertEquals(dbtest2.strbal, "3.0");
    }
}
