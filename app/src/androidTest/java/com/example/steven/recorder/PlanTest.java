package com.example.steven.recorder;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PlanTest {
    private DatabaseHandler dbtest3;

    @Before
    public void setBill() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        dbtest3 = new DatabaseHandler(context);
    }

    @Test
    public void get() {
        dbtest3.get(10.0);
        assertEquals(dbtest3.ResultOfAvaiableAmount, "-7.0");
    }
}