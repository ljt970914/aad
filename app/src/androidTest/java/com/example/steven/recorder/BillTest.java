package com.example.steven.recorder;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
//It seems that the modification to build.gradle in our Lab Sheet (I use it in my testing)is the old version
//So the system reports 4 warning after building
//I didn't change it because it does not affect to run and test

//The DatabaseHandlerTest must be run firstly (because the database should be created firstly )before doing the CheckInsertTest, PlanTest and SumTest
@RunWith(AndroidJUnit4.class)
@SmallTest
public class BillTest {
    private Bill bill;

    @Test
    public void getType() {
        bill = new Bill("+", "TestName", "01/01/1970", 1000.0, "TestRemark");
        assertEquals(bill.getType(), "+");
        assertEquals(bill.getName(), "TestName");
        assertEquals(bill.getPrice(), 1000.0, 0.0);
        assertEquals(bill.getDate(), "01/01/1970");
        assertEquals(bill.getRemark(), "TestRemark");
    }
}