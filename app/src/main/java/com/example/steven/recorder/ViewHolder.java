package com.example.steven.recorder;


import android.widget.TextView;

public class ViewHolder {
    public TextView type;
    public TextView name;
    public TextView date;
    public TextView price;
    public TextView remark;
}
