package com.example.steven.recorder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.PrivateKey;

public class Accounting extends AppCompatActivity {
    private EditText name;
    private EditText date;
    private EditText price;
    private EditText remark;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounting);
        initView();
    }

    private void initView() {
        name = (EditText) findViewById(R.id.inputName);
        date = (EditText) findViewById(R.id.inputDate);
        price = (EditText) findViewById(R.id.inputPrice);
        remark = (EditText) findViewById(R.id.inputRemark);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBuy:
                Toast.makeText(this, "You pay " + price.getText().toString() + " dollars for " + name.getText().toString() + " on " + date.getText().toString() + ". Remarks: " + remark.getText().toString(), Toast.LENGTH_LONG).show();
                String aName = name.getText().toString();
                String aDate = date.getText().toString();
                String str = price.getText().toString();
                Double aPrice = Double.parseDouble(str);
                String aRemark = remark.getText().toString();
                DatabaseHandler db = new DatabaseHandler(this);
                db.addBill(new Bill("-", aName, aDate, aPrice, aRemark));
                break;

            case R.id.buttonSell:
                Toast.makeText(this, "You get " + price.getText().toString() + " dollars for " + name.getText().toString() + " on " + date.getText().toString() + ". Remarks: " + remark.getText().toString(), Toast.LENGTH_LONG).show();
                String anName = name.getText().toString();
                String anDate = date.getText().toString();
                String str2 = price.getText().toString();
                Double anPrice = Double.parseDouble(str2);
                String anRemark = remark.getText().toString();
                DatabaseHandler db2 = new DatabaseHandler(this);
                db2.addBill(new Bill("+", anName, anDate, anPrice, anRemark));
                db2.getReadableDatabase();
                break;

        }
    }
}





