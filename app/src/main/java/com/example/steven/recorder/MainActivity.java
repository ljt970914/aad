package com.example.steven.recorder;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public String strIncome;
    @Override
    protected void onResume() {
        super.onResume();
        sum();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Please Click Left Menu to Start", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        sum();
    }


    public void sum() {
        double sumIncome = 0.0;
        DatabaseHandler dbHelper1 = new DatabaseHandler(this);
        SQLiteDatabase database1 = dbHelper1.getReadableDatabase();
        Cursor cur1 = database1.rawQuery("SELECT SUM(colPrice)  FROM BillTable WHERE colType='+'", null);
        if (cur1 != null) {
            if (cur1.moveToFirst()) {
                do {
                    sumIncome = cur1.getDouble(0);
                } while (cur1.moveToNext());
            }
        }
        strIncome = Double.toString(sumIncome);
        TextView results1 = (TextView) findViewById(R.id.ShowIncome);
        results1.setText(strIncome);

        TextView results2 = (TextView) findViewById(R.id.ShowExpen);
        double sumExpen = 0.0;
        DatabaseHandler dbHelper2 = new DatabaseHandler(this);
        SQLiteDatabase database2 = dbHelper2.getReadableDatabase();
        Cursor cur2 = database1.rawQuery("SELECT SUM(colPrice) FROM BillTable WHERE colType='-'", null);
        if (cur2 != null) {
            if (cur2.moveToFirst()) {
                do {
                    sumExpen = cur2.getDouble(0);
                } while (cur2.moveToNext());
            }
        }
        String strDou = Double.toString(sumExpen);
        results2.setText(strDou);

        TextView results3 = (TextView) findViewById(R.id.ShowBalance);
        double balance = sumIncome - sumExpen;
        String strBalance = Double.toString(balance);
        results3.setText(strBalance);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_accounting) {
            // Handle the camera action
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, Accounting.class);
            startActivity(intent);

        } else if (id == R.id.nav_plan) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, Plan.class);
            startActivity(intent);

        } else if (id == R.id.nav_list) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, Result.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

