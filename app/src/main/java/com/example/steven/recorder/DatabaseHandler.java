package com.example.steven.recorder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHandler extends SQLiteOpenHelper {
    String strIncome;
    String strExpen;
    Double balance;
    String strbal;
    Context mContext;
    String ResultOfName;
    String ResultOfAvaiableAmount;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE BillTable (colType, colName, colDate, colPrice,colRemark)");
    }


    public DatabaseHandler(Context context) {
        super(context, "BillDB", null, 1);
        mContext = context;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void addBill(Bill bill) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("colType", bill.getType());
        contentValues.put("colName", bill.getName());
        contentValues.put("colDate", bill.getDate());
        contentValues.put("colPrice", bill.getPrice());
        contentValues.put("colRemark", bill.getRemark());
        long result = sqLiteDatabase.insert("BillTable", null, contentValues);
        if (result > 0) {
            Log.d("dbhelper", "inserted successfully");// for check if insert is successful
        } else {
            Log.d("dbhelper", "failed to insert");
        }
    }

    //The following content is only used for doing Test, not called when RUN APP normally
    //To easier to test all functions, I copied the other methods I wrote in other classes
    //sum(double sumIncome) -- MainActivity.sum()
    //get(double LiBalance) -- Plan.calculate()/Plan.get()
    //Because the classes which extends AppCompatActivity are not easily allowed to call when doing tests
    public void check(int id) {
        DatabaseHandler dbHelper0 = new DatabaseHandler(mContext);
        SQLiteDatabase database0 = dbHelper0.getReadableDatabase();
        Cursor cur0 = database0.rawQuery("SELECT colName From BillTable Where colType='+' and colDate='1/1/2019'and colPrice=4.0 and colRemark='TestItem1'", null);
        if (cur0 != null) {
            if (cur0.moveToFirst()) {
                do {
                    ResultOfName = cur0.getString(0);
                } while (cur0.moveToNext());
            }
        }
    }

    public void sum(double sumIncome) {
        sumIncome = 0.0;
        DatabaseHandler dbHelper1 = new DatabaseHandler(mContext);
        SQLiteDatabase database1 = dbHelper1.getReadableDatabase();
        Cursor cur1 = database1.rawQuery("SELECT SUM(colPrice)  FROM BillTable WHERE colType='+'", null);
        if (cur1 != null) {
            if (cur1.moveToFirst()) {
                do {
                    sumIncome = cur1.getDouble(0);
                } while (cur1.moveToNext());
            }
        }
        strIncome = Double.toString(sumIncome);

        double sumExpen = 0.0;
        DatabaseHandler dbHelper2 = new DatabaseHandler(mContext);
        SQLiteDatabase database2 = dbHelper2.getReadableDatabase();
        Cursor cur2 = database1.rawQuery("SELECT SUM(colPrice) FROM BillTable WHERE colType='-'", null);
        if (cur2 != null) {
            if (cur2.moveToFirst()) {
                do {
                    sumExpen = cur2.getDouble(0);
                } while (cur2.moveToNext());
            }
        }
        strExpen = Double.toString(sumExpen);
        double balance = sumIncome - sumExpen;
        strbal = Double.toString(balance);
    }

    public void get(double LiBalance) {
        double sumIncome = 0.0;
        DatabaseHandler dbHelper1 = new DatabaseHandler(mContext);
        SQLiteDatabase database1 = dbHelper1.getReadableDatabase();
        Cursor cur1 = database1.rawQuery("SELECT SUM(colPrice) AS PriceTotal FROM BillTable WHERE colType='+'", null);
        if (cur1 != null) {
            if (cur1.moveToFirst()) {
                do {
                    sumIncome = cur1.getDouble(0);
                } while (cur1.moveToNext());
            }
        }
        double sumExpen = 0.0;
        DatabaseHandler dbHelper2 = new DatabaseHandler(mContext);
        SQLiteDatabase database2 = dbHelper2.getReadableDatabase();
        Cursor cur2 = database1.rawQuery("SELECT SUM(colPrice) FROM BillTable WHERE colType='-'", null);

        if (cur2 != null) {
            if (cur2.moveToFirst()) {
                do {
                    sumExpen = cur2.getDouble(0);
                } while (cur2.moveToNext());
            }
        }
        balance = sumIncome - sumExpen;
        double AvailableAmount2 = balance - LiBalance;
        ResultOfAvaiableAmount = Double.toString(AvailableAmount2);
    }
}