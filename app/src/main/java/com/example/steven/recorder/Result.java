package com.example.steven.recorder;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Result extends AppCompatActivity {
    private EditText nameText;
    private EditText dateText;
    private EditText priceText;
    private EditText remarkText;
    private TextView Income;
    private List<Bill> billList;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase db;
    public MyAdapter myAdapter;
    TextView results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        setContentView(R.layout.app_bar_main);
        setContentView(R.layout.activity_result);

        databaseHandler = new DatabaseHandler(this);
        db = databaseHandler.getWritableDatabase();
        billList = new ArrayList<Bill>();

        show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
                clearTable();
                break;
            default:
                break;
        }
        return true;
    }

    public void clearTable() {
        SQLiteDatabase database = databaseHandler.getWritableDatabase();
        database.execSQL("delete from " + "BillTable");
        Toast.makeText(this, "Please exit the current interface to refresh", Toast.LENGTH_SHORT).show();
    }

    public void show() {
        DatabaseHandler dbHelper = new DatabaseHandler(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String table = "BillTable";

        Cursor cursor = db.query(table, null, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String type = cursor.getString(0);
            String name = cursor.getString(1);
            String date = cursor.getString(2);
            double price = cursor.getDouble(3);
            String remark = cursor.getString(4);

            Bill item = new Bill(type, name, date, price, remark);
            billList.add(item);
        }
        ListView lv = (ListView) findViewById(R.id.tv_contact_list_view);
        myAdapter = new MyAdapter(Result.this, R.layout.list, billList);
        lv.setAdapter(myAdapter);
    }
}

class MyAdapter extends ArrayAdapter<Bill> {
    private Context context;
    private LayoutInflater inflater;
    public int resourceId = 0;

    public MyAdapter(Context context, int id, List<Bill> objects) {
        super(context, id, objects);
        resourceId = id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Bill p = getItem(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, null);
            viewHolder.type = (TextView) convertView.findViewById(R.id.tv_type);
            viewHolder.name = (TextView) convertView
                    .findViewById(R.id.tv_name);
            viewHolder.date = (TextView) convertView
                    .findViewById(R.id.tv_date);
            viewHolder.price = (TextView) convertView
                    .findViewById(R.id.tv_price);
            viewHolder.remark = (TextView) convertView
                    .findViewById(R.id.tv_remark);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.type.setText(p.getType());
        viewHolder.name.setText(p.getName());
        viewHolder.date.setText(p.getDate());
        viewHolder.price.setText("" + p.getPrice());
        viewHolder.remark.setText(p.getRemark());

        return convertView;
    }
}

