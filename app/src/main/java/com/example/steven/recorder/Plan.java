package com.example.steven.recorder;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Plan extends AppCompatActivity {
    EditText LimitExpen;
    EditText LimitBalance;
    TextView AvailableAmount;
    public double balance;
    public String strbal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        intView();
        calculate();


    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setPlan:
                get();
                break;
            default:
                break;
        }
        return true;
    }

    public void intView() {
        LimitExpen = (EditText) findViewById(R.id.inputPlanExpen);
        LimitBalance = (EditText) findViewById(R.id.inputBalancePlan);
        AvailableAmount = (TextView) findViewById(R.id.Remaining);
    }

    public void calculate() {
        Double sumIncome = 0.0;
        DatabaseHandler dbHelper1 = new DatabaseHandler(this);
        SQLiteDatabase database1 = dbHelper1.getReadableDatabase();
        Cursor cur1 = database1.rawQuery("SELECT SUM(colPrice) AS PriceTotal FROM BillTable WHERE colType='+'", null);
        if (cur1 != null) {
            if (cur1.moveToFirst()) {
                do {
                    sumIncome = cur1.getDouble(0);
                } while (cur1.moveToNext());
            }
        }
        double sumExpen = 0.0;
        DatabaseHandler dbHelper2 = new DatabaseHandler(this);
        SQLiteDatabase database2 = dbHelper2.getReadableDatabase();
        Cursor cur2 = database1.rawQuery("SELECT SUM(colPrice) FROM BillTable WHERE colType='-'", null);

        if (cur2 != null) {
            if (cur2.moveToFirst()) {
                do {
                    sumExpen = cur2.getDouble(0);
                } while (cur2.moveToNext());
            }
        }
        balance = sumIncome - sumExpen;
        strbal = Double.toString(balance);
    }


    public void get() {
        String str = LimitBalance.getText().toString();
        double LiBalance = Double.parseDouble(str);
        double AvailableAmount2 = balance - LiBalance;
        String strReAmount = Double.toString(AvailableAmount2);
        AvailableAmount.setText(strReAmount);

        if (AvailableAmount2 < 100 && AvailableAmount2 >= 0) {
            Toast.makeText(this, "You are about to overspend !", Toast.LENGTH_LONG).show();
        }
        if (AvailableAmount2 < 0) {
            Toast.makeText(this, "You have overspend !", Toast.LENGTH_LONG).show();
        }
    }

}
