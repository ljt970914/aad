package com.example.steven.recorder;

public class Bill {
    private String type;
    private String name;
    private String date;
    private double price;
    private String remark;

    @Override
    public String toString() {
        return type + "," + name + "," + date + "," + price + "," + remark;
    }

    public Bill(String type, String name, String date, double price, String remark) {
        super();
        this.type = type;
        this.name = name;
        this.date = date;
        this.price = price;
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType() {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


}
